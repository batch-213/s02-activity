<?php

function doWhileLoop(){
	$count = 0;

	do{
		if ($count % 5 == 0) {
		 echo $count . ', ';
		}
		$count ++;
	} while ($count <= 1000);
};

$students = [];