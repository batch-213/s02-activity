<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S02-Activity</title>
</head>
<body>
	<h2>Divisibles of Five</h2>

	<?php doWhileLoop(); ?>

	<h2>Array Manipulation</h2>

	<?php array_push($students, 'John Smith')?>
	<pre><?php var_dump($students)?></pre>
	<pre><?php echo count($students) ?></pre>

	<?php array_push($students, 'Jane Smith')?>
	<pre><?php var_dump($students)?></pre>
	<pre><?php echo count($students) ?></pre>
	
	<?php array_shift($students)?>
	<pre><?php var_dump($students)?></pre>
	<pre><?php echo count($students) ?></pre>
</body>
</html>